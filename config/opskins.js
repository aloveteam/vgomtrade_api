const mainConfig = require('./global');

const config = {
	'apiKey': '0a013899d345e094b19e6aa42f8096', // Opskins.com api key
	'returnURL': 'vgomtrade://login/', // Your return route when user is successfully logged in
	'scopes': 'identity items', // Scopes to limit what a token may have access to in a user's account
	'isMobile': true, // Check if app is mobile so not show navbar on opskins login
	'secret2FA': 'NJ6RSYTOXUNLPCVD', // Opskins 2FA bot secret
	'botFee': 0.1, // Bot fee on trades
	'minItemPrice': 0.15 // Minimum acceptable item
};

module.exports = config;
