'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Trades', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      trade_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      sender_opskins_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      sender_opskins_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      recipient_opskins_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      state: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      state_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      state_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      time_expires: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      message: {
        type: Sequelize.STRING,
        allowNull: false
      },
      message_code: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Trades');
  }
};
