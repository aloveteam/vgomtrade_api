const request = require('request');
const crypto = require('crypto');

function init(obj) {
	let _this = this;
	if (!obj.name || !obj.returnURL || !obj.apiKey) {
		throw new Error('Missing name, returnURL or apiKey parameter. These are required.');
	}
	else {
		let apiKey = Buffer.from(obj.apiKey + ':', 'ascii').toString('base64');
		let options = {
			url: 'https://api.opskins.com/IOAuth/GetOwnedClientList/v1/'
			, headers: {
				'authorization': `Basic ${apiKey}`
				, 'Content-Type': 'application/json; charset=utf-8'
			}
		};
		request.get(options, (err, response, body) => {
			if (err) {
				console.error(err);
			}
			else {
				let notErr = true;
				try {
					body = JSON.parse(body);
				}
				catch (e) {
					console.error(e);
					notErr = false;
				}
				finally {
					if (notErr) {
						body.response.clients.forEach(function (client) {
							if (client.name == obj.name) {
                let options = {
                  url: 'https://api.opskins.com/IOAuth/DeleteClient/v1/'
                  , headers: {
                    'authorization': `Basic ${apiKey}`
                    , 'Content-Type': 'application/x-www-form-urlencoded'
                  }
                  , body: `client_id=${client.client_id}`
                };
                request.post(options, (err, response, body) => {
                  if (err) {
                    console.error(err);
                  }
                });
							}
						});

            let options = {
              url: 'https://api.opskins.com/IOAuth/CreateClient/v1/'
              , headers: {
                'authorization': `Basic ${apiKey}`
                , 'Content-Type': 'application/json; charset=utf-8'
              }
              , body: `{"name": "${obj.name}", "redirect_uri": "${obj.returnURL}"}`
            };
            request.post(options, (err, response, body) => {
              if (err) {
                console.error(err);
              }
              else {
                let noParseErr = true;
                try {
                  body = JSON.parse(body);
                }
                catch (e) {
                  console.error(e);
                }
                finally {
                  if (noParseErr) {
                    if (body.response && body.response.client && body.response.client.client_id && body.response.secret) {
                      _this.clientID = body.response.client.client_id;
                      _this.clientSecret = body.response.secret;
                      _this.scope = obj.scopes || 'identity';
                      _this.states = [];
                      _this.mobileStr = ``;
                      if (obj.mobile) {
                        _this.mobileStr = `&mobile=1`;
                      }
                      _this.permanentStr = ``;
                      if (obj.permanent) {
                        _this.permanentStr = `&duration=permanent`;
                      }
                    }
                    else {
                      throw new Error(body.message);
                    }
                  }
                }
              }
            });
					}
				}
			}
		});

		this.getFetchUrl = () => {
			const rand = crypto.randomBytes(4).toString('hex');
			_this.states.push(rand);
			return `https://oauth.opskins.com/v1/authorize?state=${rand}&client_id=${_this.clientID}&response_type=code&scope=${_this.scope}${_this.mobileStr}${_this.permanentStr}`;
			setTimeout(function () {
				for (let i = 0; i < _this.states.length; i++) {
					if (_this.states[i] == rand) {
						_this.states.splice(i, 1);
					}
				}
			}, 300000);
		};

		this.getClientID = () => {
			return _this.clientID;
		}

		this.getClientSecret = () => {
			return _this.clientSecret;
		}

	}
}

module.exports = {
	init
}
