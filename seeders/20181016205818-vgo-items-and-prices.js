'use strict';
const axios = require('axios');
// Configs
const config = require('./../config/global');
const configOpskins = require('./../config/opskins');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return axios.get('https://api-trade.opskins.com/IItem/GetItems/v1/?key=' + configOpskins.apiKey).then((response) => {
      let items = [];
      for(let key in response.data.response.items) {
        for(let key2 in response.data.response.items[key]) {
          delete response.data.response.items[key][key2].paint_index;
          delete response.data.response.items[key][key2].wear_tier_index;
          delete response.data.response.items[key][key2].suggested_price_floor;

          let image300 = response.data.response.items[key][key2].image['300px'];
          let image600 = response.data.response.items[key][key2].image['600px'];

          delete response.data.response.items[key][key2].image;

          response.data.response.items[key][key2].image300 = image300;
          response.data.response.items[key][key2].image600 = image600;

          response.data.response.items[key][key2].suggested_price /= 100;

          items.push(response.data.response.items[key][key2]);
        }
      }

      return queryInterface.bulkInsert('Items', items, {});
    });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Items', null, {});
  }
};
