'use strict';
module.exports = (sequelize, DataTypes) => {
  var Trade = sequelize.define('Trade', {
    trade_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    sender_opskins_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    sender_opskins_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    recipient_opskins_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    state: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    state_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    state_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    time_expires: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false
    },
    message_code: {
      type: DataTypes.STRING,
      allowNull: false
    }
  });

  return Trade;
};
