'use strict';
module.exports = (sequelize, DataTypes) => {
  var Bot = sequelize.define('Bot', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    avatar: {
      type: DataTypes.STRING,
      allowNull: false
    },
    apiKey: {
      type: DataTypes.STRING
    },
    secret2FA: {
      type: DataTypes.STRING
    }
  });

  return Bot;
};
