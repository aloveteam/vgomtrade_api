'use strict';
module.exports = (sequelize, DataTypes) => {
  var Item = sequelize.define('Item', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    category: {
      type: DataTypes.STRING
    },
    rarity: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.STRING
    },
    color: {
      type: DataTypes.STRING
    },
    image300: {
      type: DataTypes.STRING,
      allowNull: false
    },
    image600: {
      type: DataTypes.STRING,
      allowNull: false
    },
    suggested_price: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false
    }
  });

  return Item;
};
