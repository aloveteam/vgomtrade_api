const express = require('express');
const router = express.Router();
const passport = require('passport');
const CustomStrategy = require('passport-custom');
const opAuth = require('../helpers/opskinsAuth');
const jwt = require('jsonwebtoken');
const request = require('request');
const crypto = require('crypto');
const qs = require('qs');
const axios = require('axios');

// Configs
const config = require('./../config/global');
const configOpskins = require('./../config/opskins');

// Models
const models = require('./../models');
let User = models.User;
let Trade = models.Trade;

// Define OPSkinsAuth
let OPSkinsAuth = new opAuth.init({
  name: config.appName,
  returnURL: configOpskins.returnURL,
  apiKey: configOpskins.apiKey,
  scopes: configOpskins.scopes,
  mobile: configOpskins.isMobile
});

router.get('/opskins', (req, res) => {
  res.json({
    url: OPSkinsAuth.getFetchUrl()
  });
});

router.post('/opskins/authenticate',
  (req, res) => {
    if (!req.body.code || !req.body.state) {
      res.json({
        success: false,
        message: 'Code or state not provided.'
      })
    }

    let auth = 'Basic ' + Buffer.from(OPSkinsAuth.getClientID() + ':' + OPSkinsAuth.getClientSecret()).toString('base64');
    let headers = {
      'Authorization': auth,
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    let options = {
      url: 'https://oauth.opskins.com/v1/access_token',
      method: 'POST',
      headers: headers,
      body: `grant_type=authorization_code&code=${req.body.code}`
    };

    request.post(options, (err, response, body) => {
      if (err) {
        res.json({
          success: false,
          message: err
        })
      } else {
        let noparseErr2 = true;
        try {
          body = JSON.parse(body)
        } catch (e) {
          res.json({
            success: false,
            message: e
          })
          noErr = false;
        } finally {
          if (noparseErr2) {
            var token = body.access_token;
            let headers2 = {
              'Authorization': `Bearer ${token}`
            };
            let options2 = {
              url: 'https://api.opskins.com/IUser/GetProfile/v1/',
              headers: headers2
            };
            request.get(options2, (err, response, body3) => {
              if (err) {
                res.json({
                  success: false,
                  message: err
                })
              } else {
                let noErr = true;
                let realBody;
                try {
                  realBody = JSON.parse(body3);
                } catch (e) {
                  res.json({
                    success: false,
                    message: e
                  })
                  noErr = false;
                } finally {
                  if (noErr) {
                    if (realBody.status == 1) {
                      let user = realBody.response;
                      user.access_token = token;

                      User.findOne({
                        where: {
                          opskins_id: user.id
                        }
                      }).then((user1) => {
                        var user1 = user1;
                        if (user1) {
                          user1.update({
                            username: user.username,
                            avatar: user.avatar,
                            access_token: user.access_token
                          }).then((user2) => {
                            const payload = user1.id;
                            const secret = config.jwtSecret;

                            let token = jwt.sign(payload, secret);

                            res.json({
                              success: true,
                              token: token,
                              user: user1,
                              message: 'Successfully logged in.'
                            });
                          }).catch((err) => {
                            res.json({
                              success: false,
                              message: err
                            });
                          });
                        } else {
                          User.create({
                            opskins_id: user.id,
                            username: user.username,
                            avatar: user.avatar,
                            access_token: user.access_token
                          }).then((user2) => {
                            const payload = user2.id;
                            const secret = config.jwtSecret;

                            let token = jwt.sign(payload, secret);

                            res.json({
                              success: true,
                              token: token,
                              user: user2,
                              message: 'Successfully logged in.'
                            });
                          }).catch((err) => {
                            res.json({
                              success: false,
                              message: err
                            });
                          });
                        }
                      }).catch((err) => {
                        res.json({
                          success: false,
                          message: err
                        });
                      });
                    } else {
                      res.json({
                        success: false,
                        message: `Failed to serialize user into session: ${realBody.error}`
                      })
                    }
                  } else {
                    res.json({
                      success: false,
                      message: `Failed to serialize user into session: ${realBody.error}`
                    })
                  }
                }
              }
            });
          }
        }
     }
  })
});

module.exports = router;
