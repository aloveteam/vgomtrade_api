const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const axios = require('axios');

// Configs
const config = require('./../config/global');
const configOpskins = require('./../config/opskins');

// Models
const models = require('./../models');
let User = models.User;
let Item = models.Item;

function roundToTwo(num) {
	return +(Math.round(num + "e+2") + "e-2");
}

router.get('/me', function (req, res) {
	let token = '';
	if (req.headers.authorization) {
		token = req.headers.authorization.split(' ')[1];
	}

	jwt.verify(token, config.jwtSecret, (err, decoded) => {
		if (err) {
			res.json({
				success: false,
				message: err.message
			});
		} else {
			User.findOne({
				id: decoded
			}).then((user) => {
				if (user) {
					res.json({
						success: true,
						user: user,
						message: 'User successfully found!'
					});
				} else {
					res.json({
						success: false,
						message: 'Cannot find user with that ID in our database.'
					});
				}
			});
		}
	});
});

router.get('/inventory', function (req, res) {
	let token = '';
	if (req.headers.authorization) {
		token = req.headers.authorization.split(' ')[1];
	}

	jwt.verify(token, config.jwtSecret, (err, decoded) => {
		if (err) {
			res.json({
				success: false,
				message: err.message
			});
		} else {
			User.findOne({
				id: decoded
			}).then((user) => {
				if (user) {
					let headers = {
						'Authorization': 'Bearer ' + user.access_token
					};

					return axios.get('https://api-trade.opskins.com/IUser/GetInventory/v1/?app_id=1&sort=6', {
						headers: headers
					}).then((response) => {
						if (response.error) {
							if (response.error == 'invalid_token') {
								res.json({
									success: false,
									message: 'OPSkins access token has expired. Please log in again.'
								});
							}

							res.json({
								success: false,
								message: response.error_description
							});
						} else {
							for (let key in response.data.response.items) {
									let newPrice = roundToTwo(response.data.response.items[key].suggested_price / 100);
									response.data.response.items[key].suggested_price = newPrice;
									if(newPrice < configOpskins.minItemPrice) {
										response.data.response.items[key].available = false;
									} else {
										response.data.response.items[key].available = true;
									}
							}

							if (response.data.response.items.length > 0) {
								res.json({
									success: true,
									items: response.data.response.items
								});
							} else {
								res.json({
									success: true,
									items: response.data.response.items,
									message: 'You do not have any VGO items in your inventory.'
								});
							}
						}
					});
				} else {
					res.json({
						success: false,
						message: 'Cannot find user with that ID in our database.'
					});
				}
			});
		}
	});
});

module.exports = router;
