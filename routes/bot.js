const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const axios = require('axios');
const authenticator = require('authenticator');

// Configs
const config = require('./../config/global');
const configOpskins = require('./../config/opskins');

// Models
const models = require('./../models');
let Bot = models.Bot;
let Item = models.Item;
let Trade = models.Trade;
let User = models.User;

function roundToTwo(num) {
	return +(Math.round(num + "e+2") + "e-2");
}

function generateTwoFactorCode() {
	return authenticator.generateToken(configOpskins.secret2FA);
}

function makeCode() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < 5; i++)
	  text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

router.get('/me', function (req, res) {
	let token = '';
	if (req.headers.authorization) {
		token = req.headers.authorization.split(' ')[1];
	}

	jwt.verify(token, config.jwtSecret, (err, decoded) => {
		if (err) {
			res.json({
				success: false,
				message: err.message
			});
		} else {
			/* TODO: Add feature for more bots */
			Bot.findOne({
				id: 1
			}).then((bot) => {
				if (bot) {
					res.json({
						success: true,
						bot: bot,
						message: 'Bot successfully found!'
					});
				} else {
					res.json({
						success: false,
						message: 'Cannot find bot with that ID in our database.'
					});
				}
			});
		}
	});
});

router.get('/inventory', function (req, res) {
	let token = '';
	if (req.headers.authorization) {
		token = req.headers.authorization.split(' ')[1];
	}

	jwt.verify(token, config.jwtSecret, (err, decoded) => {
		if (false) {
			res.json({
				success: false,
				message: err.message
			});
		} else {
			/* TODO: Add feature for more bots */
			Bot.findOne({
				id: 1
			}).then((bot) => {
				if (bot) {
					return axios.get('https://api-trade.opskins.com/IUser/GetInventory/v1/?key=' + configOpskins.apiKey + '&app_id=1&sort=6').then((response) => {
						if (response.error) {
							if (response.error == 'invalid_token') {
								res.json({
									success: false,
									message: 'OPSkins access token has expired. Please log in again.'
								});
							}

							res.json({
								success: false,
								message: response.error_description
							});
						} else {
							for (let key in response.data.response.items) {
								response.data.response.items[key].suggested_price = roundToTwo((response.data.response.items[key].suggested_price / 100) * (1 + configOpskins.botFee));
							}

							if (response.data.response.items.length > 0) {
								res.json({
									success: true,
									items: response.data.response.items
								});
							} else {
								res.json({
									success: true,
									items: response.data.response.items,
									message: 'Our BOT does not have any VGO items in the inventory.'
								});
							}
						}
					});
				} else {
					res.json({
						success: false,
						message: 'Cannot find bot with that ID in our database.'
					});
				}
			});
		}
	});
});

router.post('/trade', function (req, res) {
	let token = '';
	if (req.headers.authorization) {
		token = req.headers.authorization.split(' ')[1];
	}

	jwt.verify(token, config.jwtSecret, (err, decoded) => {
		if (false) {
			res.json({
				success: false,
				message: err.message
			});
		} else {
			var items_to_send = req.body.items_to_send;
			var items_to_receive = req.body.items_to_receive;

			if (items_to_receive.length === 0) {
				res.json({
					success: false,
					message: 'You must select some items from your inventory.'
				});
			}

			var user_total_price = 0;
			var bot_total_price = 0;

			// Get user first
			User.findOne({
				id: decoded
			}).then((user) => {
				if (user) {
					var headers = {
						'Authorization': 'Bearer ' + user.access_token
					};

					// Get users opskins inventory and check prices
					return axios.get('https://api-trade.opskins.com/IUser/GetInventory/v1/?app_id=1', {
						headers: headers
					}).then((response) => {

						if (response.error) {
							if (response.error == 'invalid_token') {
								res.json({
									success: false,
									message: 'OPSkins access token has expired. Please log in again.'
								});
							}

							res.json({
								success: false,
								message: response.error_description
							});
						} else {

							for (let i = 0; i < items_to_receive.length; i++) {
								let item = response.data.response.items.find((item) => {
									return item.id === items_to_receive[i];
								});

								if (!item) {
									res.json({
										success: false,
										message: 'Some items are missing from your inventory.'
									});
								}

								user_total_price += roundToTwo(item.suggested_price / 100);
							}

							Bot.findOne({
								id: 1
							}).then((bot) => {
								if (bot) {
									return axios.get('https://api-trade.opskins.com/IUser/GetInventory/v1/?key=' + configOpskins.apiKey + '&app_id=1').then((response) => {
										if (response.error) {
											if (response.error == 'invalid_token') {
												res.json({
													success: false,
													message: 'OPSkins access token has expired. Please log in again.'
												});
											}

											res.json({
												success: false,
												message: response.error_description
											});
										} else {
											for (let i = 0; i < items_to_send.length; i++) {
												let item = response.data.response.items.find((item) => {
													return item.id === items_to_send[i];
												});

												if (!item) {
													res.json({
														success: false,
														message: 'Some items are missing from bots inventory.'
													});
												}

												bot_total_price += roundToTwo((item.suggested_price / 100) * (1 + configOpskins.botFee));
											}

											if (user_total_price < bot_total_price) {
												res.json({
													success: false,
													message: 'The value of your selected items must be greater or equal to bots selected items.'
												});
											}

											// Get trade url
											return axios.get('https://api-trade.opskins.com/ITrade/GetTradeURL/v1/', {
												headers: headers
											}).then((response) => {
												if (response.error) {
													res.json({
														success: false,
														message: response.error_description
													});
												} else {
													let trade_url = response.data.response.short_url;
													let two_factor_code = generateTwoFactorCode();
													let tradeParameters = {
														twofactor_code: two_factor_code,
														trade_url: trade_url,
														items_to_send: items_to_send.join(),
														items_to_receive: items_to_receive.join(),
														expiration_time: 900,
														message: 'Trade from VGO mTrade. Thank you for using our service!',
														key: configOpskins.apiKey
													};

													// Initiate a trade
													return axios.post('https://api-trade.opskins.com/ITrade/SendOffer/v1/', tradeParameters).then((response)=> {
														if(response.data.status !== 1) {
															res.json({
																success: false,
																message: response.data.message
															});
														} else {
															let trade = response.data.response.offer;
															let tradeInput = {
																trade_id: trade.id,
																sender_opskins_id: trade.sender.uid,
																sender_opskins_name: trade.sender.display_name,
																recipient_opskins_id: trade.recipient.uid,
																state: trade.state,
																state_name: trade.state_name,
																time_expires: trade.time_expires,
																message: trade.message,
																message_code: makeCode()
															};

															Trade.create(tradeInput).then((trade) => {
																if(trade) {
																	let io = req.app.get('io');

																	io.emit('trades count', {
													          trades: 1
													        });

																	res.json({
																		success: true,
																		trade: trade
																	});
																} else {
																	res.json({
																		success: false,
																		message: 'Cannot create trade right now.'
																	});
																}
															});
														}
													}).catch((err) => {
														res.json({
															success: false,
															message: err.response.data.message
														});
													});
												}
											});

										}
									});
								} else {
									res.json({
										success: false,
										message: 'Cannot find bot with that ID in our database.'
									});
								}
							});
						}
					});
				} else {
					res.json({
						success: false,
						message: 'Cannot find user with that ID in our database.'
					});
				}

			});

		}
	});
});

module.exports = router;
