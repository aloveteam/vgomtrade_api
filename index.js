// Configs
const env = process.env.NODE_ENV || 'development';
const config = require('./config/global');
const configDatabase = require('./config/config.json')[env];

// Packages
const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const passport = require('passport');
const Sequelize = require('sequelize');
const io = require('socket.io')(config.socketPort);
const logger = require('morgan');
const bodyParser = require('body-parser');

// Models
const models = require('./models');
let User = models.User;
let Trade = models.Trade;

// Database connection
const sequelize = new Sequelize(configDatabase.database, configDatabase.username, configDatabase.password, {
  host: configDatabase.host,
  dialect: configDatabase.dialect,
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

// Count online users
io.sockets.on("connection", socket => {
  let clientCount = io.engine.clientsCount;
  io.sockets.emit("user count", clientCount);

  Trade.count().then((trades) => {
      socket.emit('trades count', {
        trades: trades
      });
  });

  socket.on("disconnect", () => {
    io.sockets.emit("user count", clientCount);
  });
});

// Passport setup
passport.serializeUser((user, done) => {
	done(null, user._json);
});
passport.deserializeUser((user, done) => {
	done(null, user);
});

// Socket
app.set('io', io);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());

app.use(passport.initialize());
app.use(passport.session());

// Routes
const auth = require('./routes/auth');
const user = require('./routes/user');
const bot = require('./routes/bot');

// Use routes
app.use('/auth', auth);
app.use('/user', user);
app.use('/bot', bot);

// Catch 404
app.use((req, res, next) => {
	let err = new Error('Not found');
	err.status = 404;
	next(err);
});

// Error handler
app.use((err, req, res, next) => {
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// Return error
	res.status(err.status || 500);
	res.json({ error: err });
});

app.listen(config.port || 3000, () => {
	console.log('App running on port ' + config.port || 3000);
});
